class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById('cars-container')
    this.buttonCari = document.getElementById('cari-mobil')
  }

  async init() {
    await this.load()
    // Register click listener
    // this.clearButton.onclick = this.clear
    this.buttonCari.onclick = this.run
  }

  run = () => {
    let capacityCar = document.getElementById('qty').value
    let dateCari = document.getElementById('date').value
    let timeCari = document.getElementById('time').value
    let driver = document.getElementById('driver').value

    let newDate = new Date(`${dateCari}T${timeCari}Z`)
    if (
      dateCari === '' ||
      timeCari === '' ||
      driver === ''
    ) {
      alert('Isi semua form terlebih dahulu')
    } else {
      // 2022-03-23T15:49:05.563Z
      // console.log(dateCari)
      // console.log(timeCari)
      // console.log(newDate)
      // clear elemenent current
      this.clear()
      Car.list.forEach((car) => {
        if (
          car.capacity >= capacityCar &&
          car.availableAt > newDate &&
          car.available === true
        ) {
          const node = document.createElement('div')
          // console.log(car.capacity)
          node.innerHTML = car.render()
          this.carContainerElement.append(node)
        }
      })
    }
  }

  // filter = (date, cap) => {
  //   var capacity1 = cap
  //   var date1 = date
  //   console.log(capacity1)
  //   console.log(date1)
  //   Car.list.forEach((car) => {
  //     // console.log(car.capacity)
  //     if (car.availableAt > date1 && car.capacity > capacity1) {
  //       const node = document.createElement('div')
  //       node.innerHTML = car.render()
  //       this.carContainerElement.append(node)
  //       console.log('True')
  //     }
  //   })
  // }

  async load() {
    const cars = await Binar.listCars()
    Car.init(cars)
    // this.filter(date, cap)
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild

    while (child) {
      child.remove()
      child = this.carContainerElement.firstElementChild
    }
  }
}
